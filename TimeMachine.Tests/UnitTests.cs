using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TimeMachines;

namespace Tests
{
    [TestClass]
    public class UnitTests
    {
        [TestMethod]
        public void TestTaiUtcInitialOffset()
        {
            TimeMachine tm = TimeMachine.Instance;
            DateTime utc = tm.UtcNow;
            DateTime tai = tm.TaiNow;

            TimeSpan gap = tai - utc;
            Assert.AreEqual(37, Math.Floor(gap.TotalSeconds));
        }

        [TestMethod]
        public void TestSingletonCreation()
        {
            TimeMachine tm = TimeMachine.Instance;
            TimeMachine tm2 = TimeMachine.Instance;

            Assert.AreSame(tm, tm2);
        }

        [TestMethod]
        public void TestFeedbackLoop()
        {
            const int lineDelayms = 2500;

            TimeMachine tm = TimeMachine.Instance;
            tm.IsSimulatedTime = true;
            tm.SimulationRate = 0;

            DateTime desiredTransTime = tm.UtcNow;
            tm.Sleep(lineDelayms);  // The time it takes to propagate

            
            DateTime txControllerTime = tm.TaiNow;
            TimeSpan latency = desiredTransTime - txControllerTime;
            //int taiUtcOffset = tm.TaiUtcOffsetSeconds;

            Assert.IsTrue(latency.TotalSeconds < 0); // Assert that initially we are asking for Tx too late

            // Now correct MC time
            desiredTransTime = tm.UtcNow.AddSeconds(-latency.TotalSeconds).AddSeconds(1.5); // 1.5 seconds "in hand"
            tm.Sleep(lineDelayms);

            txControllerTime = tm.TaiNow;
            TimeSpan availableTime = desiredTransTime - txControllerTime;
            Assert.IsTrue(availableTime.TotalSeconds > 1, "Total seconds was: " + availableTime.TotalSeconds); // Assert that we are now generating with time to spare         
            Console.WriteLine("Message generated {0}s ahead of time", availableTime.TotalSeconds);
            
            // Now do again after a leap second
            tm.AddLeapSecond();
            desiredTransTime = tm.UtcNow.AddSeconds(-latency.TotalSeconds).AddSeconds(1.5); // 1.5 seconds "in hand"
            tm.Sleep(lineDelayms);

            txControllerTime = tm.TaiNow;
            availableTime = desiredTransTime - txControllerTime;
            Assert.IsTrue(availableTime.TotalSeconds < 1, "Second time, total seconds was: " + availableTime.TotalSeconds); // Assert that we are now generating with time to spare         

        }
        
    }
}

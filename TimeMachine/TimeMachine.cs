﻿using System;
using System.Threading;

namespace TimeMachines
{
    public sealed class TimeMachine
    {
        private static TimeMachine instance = null;
        private static readonly object padlock = new object();        

        private bool isSimulatedTime;
        private DateTime SimulatedUtcTime;
        private int taiUtcOffsetSeconds = 37;
        private double simulationRate;
        private Timer simulationTimer;

        private TimeMachine()
        {
        }

        public static TimeMachine Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new TimeMachine();
                    }
                    return instance;
                }
            }
        }

        public bool IsSimulatedTime
        {
            get { return isSimulatedTime;}
            set 
            { 
                if (value == true && isSimulatedTime == false)  // Entering Simulated Time mode
                {
                    SimulatedUtcTime = DateTime.UtcNow;
                    //if (simulationTimer is null)
                    //    simulationTimer = new Timer(SimulationTick, null, 0, (int)(1000 * simulationRate));                  
                    //else
                    //    simulationTimer.Change(0, (int)(1000 * simulationRate));                    
                }
                isSimulatedTime = value;
            }
        }

        private void SimulationTick(object state)
        {
            // A simulated second has passed
            //SimulatedUtcTime.AddSeconds(1);
        }

        public int TaiUtcOffsetSeconds 
        {
            get
            {
                return taiUtcOffsetSeconds;
            } 
            set
            {
                taiUtcOffsetSeconds = value;
            }
        }

        public void AddLeapSecond()
        {
            taiUtcOffsetSeconds++;
        }

        public DateTime UtcNow 
        {
            get
            {
                if (IsSimulatedTime)
                    return SimulatedUtcTime;
                else
                    return DateTime.UtcNow;
            } 
        }

        public DateTime TaiNow 
        {
            get
            {
                if (IsSimulatedTime)
                    return SimulatedUtcTime.AddSeconds(TaiUtcOffsetSeconds);
                else
                    return DateTime.UtcNow.AddSeconds(TaiUtcOffsetSeconds);
            } 
        }      

        public double SimulationRate
        {
            get
            {
                return simulationRate;
            }
            set
            {
                simulationRate = value;
            }
        }

        public void Sleep(int milliseconds)
        {
            if (isSimulatedTime)
            {
                if (simulationRate > 0)
                    Thread.Sleep((int)(milliseconds * simulationRate));
                SimulatedUtcTime.AddMilliseconds(milliseconds);
            }
            else
            {                
                Thread.Sleep(milliseconds);
            }

        }
    }
}
